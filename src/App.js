import React, { Component } from 'react';
import {Switch, Route} from "react-router-dom";

import Layout from "./components/Layout/Layout";
import MainContent from "./containers/MainContent/MainContent";
import Admin from "./containers/Admin/Admin";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path="/" exact component={MainContent} />
          <Route path="/pages/admin" exact component={Admin} />
          <Route path="/pages/:page" component={MainContent} />
        </Switch>
      </Layout>
    );
  }
}

export default App;