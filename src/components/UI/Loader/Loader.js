import React from 'react';
import './Loader.css';
import loaderImg from '../../../assets/images/preloader.svg';

const Loader = () => (
  <div className="Loader">
    <img src={loaderImg} alt=""/>
  </div>
);

export default Loader;