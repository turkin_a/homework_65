import React from 'react';
import './Button.css';

const Button = props => {
  return <button
    className="Btn"
    onClick={(e) => props.clicked(e)}
  >{props.children}</button>;
};

export default Button;