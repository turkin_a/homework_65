import React from 'react';
import './NavigationItems.css';
import NavigationItem from "./NavigationItem/NavigationItem";
import pages from "../../../PagesIsIncluded";

const NavigationItems = () => (
  <ul className="NavigationItems">
    {Object.keys(pages).map(page => (
      <NavigationItem key={page} to={`/pages/${pages[page]}`}>{page}</NavigationItem>
    ))}
    <NavigationItem to="/pages/admin">Admin</NavigationItem>
  </ul>
);

export default NavigationItems;