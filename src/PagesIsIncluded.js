const pages = {
  'Home': 'home',
  'Products': 'products',
  'Support': 'support',
  'About': 'about',
  'Contacts': 'contacts',
  'Add test': 'test1',
  'Add test2': 'test2'
};

export default pages;