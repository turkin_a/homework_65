import React, {Component, Fragment} from 'react';
import './MainContent.css';
import axios from '../../axios-orders';
import Loader from "../../components/UI/Loader/Loader";

class MainContent extends Component {
  state = {
    title: '',
    content: '',
    currentPage: '',
    databasePagesId: '-L6V8pqZb278G3Z2IlX-',
    loading: true
  };

  getContent = currentPage => {
    axios.get(`pages/${this.state.databasePagesId}/${currentPage}.json`).then(response => {
      let title = 'Page is empty';
      let content = 'Go to Admin panel to add new content';
      if (response.data) {
        title = response.data.title;
        content = response.data.content;
      }
      this.setState({title, content, currentPage, loading: false});
    });
  };

  componentDidMount() {
    if (!this.props.match.params.page) this.props.history.replace('/pages/home');
    let currentPage = this.props.match.params.page || 'home';
    this.getContent(currentPage);
  }

  componentDidUpdate() {
    let currentPage = this.props.match.params.page;
    if (this.state.loading) this.getContent(currentPage);
    if (currentPage !== this.state.currentPage) this.setState({currentPage, loading: true});
  }

  render() {
    let pageContent = (
      <Fragment>
        <h2 className="MainTitle">{this.state.title}</h2>
        <div className="MainBody">{this.state.content}</div>
      </Fragment>
    );

    if (this.state.loading) pageContent = <Loader />;

    return (
      <div className="MainContent">
        {pageContent}
      </div>
    );
  }
}

export default MainContent;