import React, {Component, Fragment} from 'react';
import './Admin.css';
import axios from '../../axios-orders';
import Loader from "../../components/UI/Loader/Loader";
import Button from "../../components/UI/Button/Button";
import pages from "../../PagesIsIncluded";

class Admin extends Component {
  state = {
    page: '',
    title: '',
    content: '',
    databasePagesId: '-L6V8pqZb278G3Z2IlX-',
    loading: false
  };

  getContent = currentPage => {
    this.setState({loading: true});
    axios.get(`pages/${this.state.databasePagesId}/${currentPage}.json`).then(response => {
      let title = '';
      let content = '';
      if (response.data) {
        title = response.data.title;
        content = response.data.content;
      }
      this.setState({page: currentPage, title, content, loading: false});
    });
  };

  changeFieldHandler = event => {
    const field = event.target.name;
    const value = event.target.value;
    if (field === 'pages') this.getContent(pages[value]);
    else this.setState({[field]: value, loading: false});
  };

  submitForm = (event) => {
    event.preventDefault();
    axios.put(`pages/${this.state.databasePagesId}/${this.state.page}.json`, {title: this.state.title, content: this.state.content})
      .finally(() => {
        this.props.history.replace(`/pages/${this.state.page}`);
      });
  };

  cancelForm = (event) => {
    event.preventDefault();
    this.props.history.replace('/pages/home');
  };

  componentDidMount() {
    this.getContent('home');
  }

  render() {
    let formFields = (
      <Fragment>
        <label>Edit a title</label>
        <input type="text" name="title" className="FormField TitleInput"
               value={this.state.title}
               onChange={(e) => this.changeFieldHandler(e)}
        />
        <label>Edit a content</label>
        <textarea name="content" className="FormField ContentField"
                  value={this.state.content}
                  onChange={(e) => this.changeFieldHandler(e)}
        />
        <div className="Buttons">
          <Button clicked={this.submitForm}>Save</Button>
          <Button clicked={this.cancelForm}>Cancel</Button>
        </div>
      </Fragment>
    );

    if (this.state.loading) formFields = <Loader />;

    return (
      <div className="Admin">
        <h2 className="AdminTitle">Edit content</h2>
        <div className="AdminBody">
          <form className="EditForm">
            <label>Select the page</label>
            <select name="pages" className="FormField SelectMenu" onChange={(e) => this.changeFieldHandler(e)}>
              {Object.keys(pages).map(page => (
                <option key={page} value={page}>{page}</option>
              ))}
            </select>
            {formFields}
          </form>
        </div>
      </div>
    );
  }
}

export default Admin;